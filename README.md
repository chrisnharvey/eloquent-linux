# Eloquent Linux

Linux desktop setup for space saving, efficiency and beauty.

## Software

- KDE neon
- Latte Dock
- Kvantum
- Window Buttons Applet

## Theme/Looks

- Materia KDE
- Materia GTK
- Papirus Dark (Icon Theme)
- Adwatia (Cursor Theme)

## Setup

### Kvantum

- MateriaBlur

### GTK Theme

- Materia

### Workspace Theme

- Materia

### Icon Theme

- Papirus-Dark

## Latte Dock

Use Eloquent latte layout